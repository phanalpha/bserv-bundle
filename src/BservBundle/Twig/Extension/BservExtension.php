<?php

namespace Mojomaja\Bundle\BservBundle\Twig\Extension;

class BservExtension extends \Twig_Extension
{
    /**
     * @var \Mojomaja\Component\Bserv\Client $bserv
     */
    private $bserv;


    public function __construct(\Mojomaja\Component\Bserv\Client $bserv)
    {
        $this->bserv = $bserv;
    }

    public function getName()
    {
        return 'mojomaja_bserv';
    }

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter(
                'bserv_quote',
                function ($localname, $width = 0, $height = 0, $schema = 'crop') {
                    return $this->bserv->quote($localname, $width, $height, $schema);
                }
            )
        ];
    }
}
