<?php

namespace Mojomaja\Bundle\BservBundle\Util;

class ScopedFile
{
    /**
     * @var \SplFileInfo
     */
    private $file;


    public function __construct(\SplFileInfo $file)
    {
        $this->file = $file;
    }

    public function __destruct()
    {
        unlink($this->file->getPathname());
    }

    public function __call($name, $args)
    {
        return call_user_func_array([ $this->file, $name ], $args);
    }

    public function __toString()
    {
        return (string) $this->file;
    }
}
