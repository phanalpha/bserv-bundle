<?php

namespace Mojomaja\Bundle\BservBundle\Util;

class FileUtil
{
    static public function lift()
    {
        return call_user_func_array(
            func_get_arg(0),
            array_map(function ($file) {
                if ($file !== null)
                    return new ScopedFile(
                        $file->move(
                            $file->getPath(),
                            implode('.', [ $file->getFilename(), $file->guessExtension() ])
                        )
                    );
            }, array_slice(func_get_args(), 1))
        );
    }
}
